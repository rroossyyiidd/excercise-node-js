const express = require('express');
// router
const router = express.Router();
// cart controller
const cartController = require('../controllers/carts')

router
  .route('')
  // get all carts data
  .get(cartController.index)
  // create new cart item
  .post(cartController.store)

router
  .route('/:id')
  // get detail cart item
  .get(cartController.detail)
  // update data cart item
  .patch(cartController.update)
  // delete cart item
  .delete(cartController.delete)

module.exports = router