const express = require('express');
const router = express.Router()

// import cart routes
const cartRoutes = require('./carts');

router.use('/carts', cartRoutes)

module.exports = router