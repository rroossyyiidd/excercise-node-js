const express = require('express');
// const bodyParser = require('body-parser');
const morgan = require('morgan');

const app = express();

// optional use body-parser or express.json and urlencoded
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
// app.use(bodyParser.json())

app.use(morgan('tiny')); // HTTP request logger middleware

// routes
const routes = require('./routes');
app.use(routes);

app.get('/', (req, res) => {
  return res.json('Start with /carts')
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error("Not Found");
  err.status = 404;
  return next(err);
});

// error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  return res.json({
    message: err.message,
    /*
     if we're in development mode, include stack trace (full error object)
     otherwise, it's an empty object so the user doesn't see all of that
    */
    error: app.get("env") === "development" ? err : {}
  });
});

app.listen(3000, ()=>{
  console.log('Server is listening on port 3000')
})