let carts = [] // this would ideally be a database, model
var id = 1; // this will help us identify unique item on cart

module.exports = {
  // get all carts data
  index: (req, res)=>{
    if(carts.length > 0) {
      return res.json({
        status: true,
        data: carts,
        method: req.method
      })
    }
    return res.json({
      status: false,
      message: 'Cart is empty',
      data: []
    })
  },
  // create new cart item
  store: (req, res)=>{
    carts.push({
      name: req.body.name,
      totalItems: req.body.totalItems,
      id: ++id
    })
    return res.json({
      status: true,
      data: carts,
      method: req.method
    })
  },
  // get detail cart item
  detail: (req, res)=>{
    carts.filter(cart => {
      if (cart.id === Number(req.params.id)) {
        return res.json({
          status: true,
          data: cart,
          method: req.method
        })
      }
    })
    return res.json({
      status: false,
      message: 'Item not found'
    })
  },
  // update data cart item
  update: (req, res)=>{
    carts.filter(cart => {
      if (cart.id === Number(req.params.id)) {
        cart.name = req.body.name,
        cart.totalItems = req.body.totalItems
        return res.json({
          status: true,
          data: cart,
          method: req.method,
          message: 'Cart item updated!'
        })
      }
    })
    return res.json({
      status: false,
      message: 'Item not found'
    })
  },
  // delete cart item
  delete: (req, res)=>{
    carts = carts.filter(val=>val.id !== Number(req.params.id))
    return res.json({
      status: true,
      data: carts,
      method: req.method,
      message: 'Cart item deleted!'
    })
  }
}