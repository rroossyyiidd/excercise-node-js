// import nodejs modules
const fs = require('fs');
const request = require('request');
// run: node main.js ${keyword} ${limit} - limit is number (optional)
var myArgs = process.argv.slice(2);
// keyword search
const keyword = myArgs[0];
// limit result search, with default 10 result limit
const limit = myArgs[1] ? myArgs[1] : 10;
// request configuration
const options = {
  url: `https://icanhazdadjoke.com/search?term=${keyword}&limit=${limit}`,
  headers: {
    'Accept': 'application/json'
  }
};
// get data
request(options, function (error, response, body) {
  const {results} = JSON.parse(body)
  if (results.length > 0) {
    let words = ''

    results.forEach((result, index) => {
      words = `${index === 0 ? '' : words + `\n\n`} ${result.joke}`
    });
    // create or update file txt which containt results of joke
    fs.writeFile('jokes.txt', words, function (err) {
      if (err) throw err;
      console.log('see result in jokes.txt file')
    });
  } else {
    // unknown or empty result
    console.log('no jokes were found for that search term.')
  }
})