 Did you hear the one about the guy with the broken hearing aid? Neither did he.

 Doctor: Do you want to hear the good news or the bad news?
Patient: Good news please.
Doctor: we're naming a disease after you.

 Did you hear about the guy who invented Lifesavers? They say he made a mint.

 Want to hear my pizza joke? Never mind, it's too cheesy.

 Did you hear about the cheese factory that exploded in France? There was nothing left but de Brie.

 Did you hear about the cow who jumped over the barbed wire fence? It was udder destruction.

 Did you hear about the scientist who was lab partners with a pot of boiling water? He had a very esteemed colleague.

 Hear about the new restaurant called Karma? There’s no menu: You get what you deserve.

 What do you call a gorilla wearing headphones? Anything you'd like, it can't hear you.

 Did you hear that the police have a warrant out on a midget psychic ripping people off? It reads “Small medium at large.”