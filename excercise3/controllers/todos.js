const db = require('../models/todos');
const {validationResult} = require('express-validator') // form validation

module.exports = {
  // get all todos data
  index: (req, res)=>{
    db.findAll().then(todos=>{
      return res.json(todos)
    })
  },
  // create new todo item
  store: (req, res)=>{
    const errors = validationResult(req)
    if(!errors.isEmpty()) {
      return res.status(422).json({errors: errors.mapped()})
    }

    db.create({
      title: req.body.title,
      note: req.body.note,
      isDone: req.body.isDone
    }).then(newData=>{
      return res.json({
        status: true,
        data: newData,
        message: 'Todo created'
      })
    })
  },
  // detail todo
  detail: (req, res)=>{
    db.findOne({where: {id: Number(req.params.id)}}).then(todo=>{
      if (todo) {
        return res.json({
          status: true,
          data: todo,
          method: req.method
        })
      }
      return res.json({
        status: false,
        message: 'Todo not found'
      })
    })
  },
  // update data todo item
  update: (req, res)=>{

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({errors: errors.mapped()});
    }
    
    const update = {
      title: req.body.title,
      note: req.body.note,
      isDone: req.body.isDone
    }

    db.update(update, {where: {id: Number(req.params.id)}})
      .then(()=>{
        return db.findOne({where: {id: Number(req.params.id)}})
      })
      .then(todo=>{
        return res.json({
          status: true,
          data: todo,
          method: req.method,
          message: 'Todo item updated'
        })
      })
  },
  // delete todo item
  delete: (req, res)=>{
    db.destroy({where: {id: Number(req.params.id)}})
      .then(affectedRows=>{
        if(affectedRows) {
          return res.json({
            status: true,
            method: req.method,
            message: 'Todo item deleted'            
          })
        }

        return res.json({
          status: false,
          message: 'Todo not deleted'
        })
      })
      .then(todo=>{
        return res.json({
          status: true,
          message: 'Todo delted',
          data: todo
        })
      })
  }
}