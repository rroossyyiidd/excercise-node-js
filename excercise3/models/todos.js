const Sequelize = require('sequelize');

const sequelize = new Sequelize('excercise3', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
})

const todos = sequelize.define('todos', {
  'id': {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  'title': Sequelize.STRING,
  'note': Sequelize.STRING,
  'isDone': Sequelize.BOOLEAN,
  'createdAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },
  'updatedAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  }
},{
  //prevent sequelize transform table name into plural
  freezeTableName: true,
  timeStamp: false
})

module.exports = todos