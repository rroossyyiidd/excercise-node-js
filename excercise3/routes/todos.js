const express = require('express');
// router
const router = express.Router();
// todo controller
const todoController = require('../controllers/todos');
// form validation
const {check} = require('express-validator');

router
  .route('')
  .get(todoController.index)
  .post([
    // form validation rule
    check('title')
      .isLength({min: 2}),
    check('note')
      .isLength({min: 2}),
    check('isDone')
      .isBoolean()
  ], todoController.store)

router
  .route('/:id')
  .get(todoController.detail)
  .patch([
    // form validation rule
    check('title')
      .isLength({min: 2}),
    check('note')
      .isLength({min: 2}),
    check('isDone')
      .isBoolean()
  ], todoController.update)
  .delete(todoController.delete)

module.exports = router